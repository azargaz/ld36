﻿using UnityEngine;
using System.Collections;

public class BombOfDarknessOnTriggerEnter : MonoBehaviour
{

    float damage;

    void Awake()
    {
        damage = gameObject.GetComponent<BombOfDarknessScript>().damage;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player" && gameObject.name.Contains("Final"))
        {
            Debug.Log(gameObject.GetComponent<BombOfDarknessScript>().damage);
            damage = gameObject.GetComponent<BombOfDarknessScript>().damage;
            collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
            Destroy(gameObject);
        }
    }
}