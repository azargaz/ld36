﻿using UnityEngine;
using System.Collections;

public class DarknessSpawnerController : MonoBehaviour
{
    public GameObject darkness;
    public float spawnCooldown;
    public float timeToLiveDarkness;
    
	void Start ()
    {
        StartCoroutine(SpawningDarkness());   
	}
	
	void Update ()
    {
	    
	}

    IEnumerator SpawningDarkness()
    {
        GameObject instance = Instantiate(darkness, transform.position, Quaternion.identity) as GameObject;
        instance.GetComponent<DarknessController>().timeToLive = timeToLiveDarkness;
        StartCoroutine(instance.GetComponent<DarknessController>().CountingToDestroy());

        yield return new WaitForSeconds(spawnCooldown);
        StartCoroutine(SpawningDarkness());
        yield return null;
    }
}