﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Setup : MonoBehaviour
{
    [SerializeField]
    GameObject gm;
    Animator menuPlayer;

    void Start ()
    {
        menuPlayer = GameObject.FindGameObjectWithTag("MenuPlayer").GetComponent<Animator>();

        if (GameObject.FindGameObjectWithTag("GameController") == null)
        {
            Instantiate(gm);
        }	
	}

    public void StartGame()
    {
        menuPlayer.SetTrigger("Choose");
        Invoke("StartGameAnim", menuPlayer.GetCurrentAnimatorStateInfo(0).length);
    }

    void StartGameAnim()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
