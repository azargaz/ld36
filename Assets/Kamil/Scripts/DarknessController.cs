﻿using UnityEngine;
using System.Collections;

public class DarknessController : MonoBehaviour
{
    public float timeToLive;
    public float damage;

	void Start ()
    {

	}
	
	void Update ()
    {
	    
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (collider.gameObject.GetComponent<PlayerController>().invulnerableToDarkness == false)
                collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
        }
    }

    public IEnumerator CountingToDestroy()
    {
        yield return new WaitForSeconds(timeToLive);
        Destroy(gameObject);
        yield return null;
    }
}