﻿using UnityEngine;
using System.Collections;

public class FlyingEnemyController : BombOfDarknessScript
{
    public float speed;
    public float bombDropCooldown;
    private int direction = 1;
    public GameObject bombOfDarkness;

    void Start ()
    {
        StartCoroutine(SpawningBomb());
	}
	
	void FixedUpdate ()
    {
        transform.Translate(new Vector2(speed * Time.deltaTime * direction, 0f));
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "ChangingDirection")
        {
            direction *= -1;
        }

        GetComponent<SpriteRenderer>().flipX = (direction == 1) ? false : true;
    }

    IEnumerator SpawningBomb()
    {
        GameObject instance = Instantiate(bombOfDarkness, transform.position, Quaternion.identity) as GameObject;
        instance.GetComponent<BombOfDarknessScript>().flyingEnemySpeed = speed;
        instance.GetComponent<BombOfDarknessScript>().bombSpeed = bombSpeed;
        instance.GetComponent<BombOfDarknessScript>().directionOfFlyingEnemy = direction;
        instance.GetComponent<BombOfDarknessScript>().damage = damage;
        instance.GetComponent<BombOfDarknessScript>().timeToDestroyExplosion = timeToDestroyExplosion;

        yield return new WaitForSeconds(bombDropCooldown);
        StartCoroutine(SpawningBomb());
        yield return null;
    }
}