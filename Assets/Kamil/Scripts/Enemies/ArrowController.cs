﻿using UnityEngine;
using System.Collections;

public class ArrowController : MonoBehaviour
{
    public float timeToLive;
    public float speed;
    public int direction;
    public float damage;
    //public LayerMask destructionMask;
    BoxCollider2D col;
    Animator anim;

    void Start ()
    {
        col = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        StartCoroutine(CountingToDestruction());
	}
	
	void FixedUpdate ()
    {
        transform.Translate(new Vector2(speed * direction * Time.deltaTime, 0f));
        GetComponent<SpriteRenderer>().flipX = (direction == 1) ? false : true;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ground")
        {
            Destroy(gameObject);
            StopAndDestroyArrow();
        }
        else if (collider.tag == "Player")
        {
            collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
            StopAndDestroyArrow();
        }
    }

    IEnumerator CountingToDestruction()
    {
        yield return new WaitForSeconds(timeToLive);
        anim.SetTrigger("Destroy");
    }

    void StopAndDestroyArrow()
    {
        speed = 0;
        col.enabled = false;
        anim.SetTrigger("Destroy");
    }

    void AnimDestroy()
    {
        Destroy(gameObject);
    }
}