﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2DController))]
public class WalkingEnemyController : MonoBehaviour
{
    Collider2DController controller;
    public float speed;
    private Vector2 movement;
    [HideInInspector] public int movementDirection = 1;
    public int startingDirection;
    float gravity = -20;

    Vector2 leftBottom;
    Vector2 rightBottom;
    public LayerMask groundMask;
    private bool attackCRisRunning = false;
    public float waitingTimeAfterAttack;
    public float damage;
    Animator anim;

    [SerializeField]
    GameObject leftAttack;
    [SerializeField]
    GameObject rightAttack;

    void Start ()
    {
        controller = gameObject.GetComponent<Collider2DController>();
        controller.verticalRayCount = 2;
        anim = gameObject.GetComponent<Animator>();
        movementDirection = -startingDirection;
	}
	
	void FixedUpdate ()
    {
        CalcuateBounds();
        RaycastHit2D leftBottomHit = Physics2D.Raycast(leftBottom, Vector2.down, 0.02f, groundMask);
        RaycastHit2D rightBottomHit = Physics2D.Raycast(rightBottom, Vector2.down, 0.02f, groundMask);

        if (controller.collisions.left || controller.collisions.right)
        {
            movementDirection *= -1;
        }
        else if (!leftBottomHit || !rightBottomHit)
        {
            movementDirection *= -1;
        }

        transform.FindChild("Invulnerable").localScale = new Vector3((movementDirection == 1) ? 1 : -1, 1, 1);
        transform.FindChild("Vulnerable").localScale = new Vector3((movementDirection == 1) ? 1 : -1, 1, 1);
        GetComponent<SpriteRenderer>().flipX = (movementDirection == 1) ? false : true;

        movement = new Vector2(speed * movementDirection, 0f);
        movement.y += gravity * Time.deltaTime;

        controller.Move(movement * Time.deltaTime);
	}

    void CalcuateBounds()
    {
        Bounds bounds = gameObject.GetComponent<BoxCollider2D>().bounds;

        leftBottom = bounds.min;
        rightBottom = new Vector2(bounds.max.x, bounds.min.y);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "PlayerForWalker")
        {
            if (attackCRisRunning == false)
                StartCoroutine(Attacking());
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "PlayerForWalker")
        {
            if (attackCRisRunning == false)
                StartCoroutine(Attacking());
        }
    }

    IEnumerator Attacking()
    {
        attackCRisRunning = true;
        //TODO -- Attacking script
        if (movementDirection == 1)
            rightAttack.SetActive(true);
        else
            leftAttack.SetActive(true);
        //Debug.Log("Walker attack");

        anim.SetTrigger("Attack");

        yield return new WaitUntil(() => anim.GetCurrentAnimatorStateInfo(0).IsName("Spikyback_attack") && //Waiting until animation is finished
            anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0));

        anim.ResetTrigger("Attack");
        yield return new WaitForSeconds(0.01f);
        anim.SetTrigger("Walk");

        rightAttack.SetActive(false);
        leftAttack.SetActive(false);

        yield return new WaitForSeconds(waitingTimeAfterAttack);

        attackCRisRunning = false;
        yield return null;
    }
}