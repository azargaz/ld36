﻿using UnityEngine;
using System.Collections;

public class BombOfDarknessScript : MonoBehaviour
{
    public float bombSpeed;
    public float flyingEnemySpeed;
    public float directionOfFlyingEnemy;
    public float timeToDestroyExplosion;
    public float damage;
    private bool stopMoving = false;
    [SerializeField]
    GameObject finalBomb;

	void Start ()
    {
	    
	}
	
	void FixedUpdate ()
    {
        if (stopMoving == false)
            transform.Translate(new Vector2(flyingEnemySpeed * Time.deltaTime / 2, bombSpeed));
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ground")
        {
            if (gameObject.name.Contains("Final"))
            {
                stopMoving = true;
                StartCoroutine(DestroyingBomb());
            }
            else
            {
                stopMoving = true;
                GameObject instance = Instantiate(finalBomb, transform.position, Quaternion.identity) as GameObject;
                instance.GetComponent<BombOfDarknessScript>().flyingEnemySpeed = 0;
                instance.GetComponent<BombOfDarknessScript>().bombSpeed = 0;
                instance.GetComponent<BombOfDarknessScript>().directionOfFlyingEnemy = 0;
                instance.GetComponent<BombOfDarknessScript>().damage = damage;
                instance.GetComponent<BombOfDarknessScript>().timeToDestroyExplosion = timeToDestroyExplosion;
                Destroy(gameObject);
            }
        }
        else if (collider.tag == "Player" && !gameObject.name.Contains("Final"))
        {
            Debug.Log(damage);
            collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
            Destroy(gameObject);
        }
    }

    IEnumerator DestroyingBomb()
    {
        yield return new WaitForSeconds(timeToDestroyExplosion);
        Destroy(gameObject);
        yield return null;
    }
}