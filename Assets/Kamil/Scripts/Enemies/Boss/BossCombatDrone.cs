﻿using UnityEngine;
using System.Collections;

public class BossCombatDrone : MonoBehaviour
{
    [SerializeField]
    GameObject spear;
    [SerializeField]
    Transform firePoint;

    [SerializeField]
    float spearCooldown;
    float timeToNextSpear;

    Vector3 direction;
    Camera mainCamera;
    [HideInInspector] public Animator anim;

    private bool dronIsMoving = false; //If false, drone is going back to player;; If true, drone is going to chosen point
    [HideInInspector] public bool dronOnTheWay = false; //Checking if drone is currently moving to player or to chosen point
    [HideInInspector] public bool movingToStandardPosition = false;
    private Vector3 localPosition;
    private Vector3 destinationPoint;
    private Transform playerPosition;
    private Transform bossPosition;
    [SerializeField]
    float droneSpeed;
    /*************/
    void Start()
    {
        anim = GetComponent<Animator>();
        localPosition = transform.localPosition;
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
        bossPosition = GameObject.FindGameObjectWithTag("Boss").transform;
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, -(playerPosition.transform.position - transform.position));

        if (dronOnTheWay == true)
        {
            if (dronIsMoving == false)
                destinationPoint = bossPosition.position;
            transform.position = Vector3.MoveTowards(transform.position, destinationPoint, droneSpeed * Time.deltaTime);
            anim.enabled = false;
            //If moving is done
            if (transform.position == destinationPoint)
            {
                dronOnTheWay = false;
                //Debug.Log("CCCP");
                anim.enabled = true;
                if (dronIsMoving == false)
                {
                    transform.parent = GameObject.FindGameObjectWithTag("Boss").transform;
                    transform.localPosition = new Vector3(0f, 0f, 0f);
                }
                else if (dronIsMoving == true)
                {
                    transform.parent = null;
                }
            }
        }

        if (movingToStandardPosition)
        {
            destinationPoint = bossPosition.position + new Vector3(-1f, 1f, 0f);
            transform.position = Vector3.MoveTowards(transform.position, destinationPoint, droneSpeed * Time.deltaTime);
            anim.enabled = false;
            if (transform.position == destinationPoint)
            {
                anim.enabled = true;
                transform.parent = GameObject.FindGameObjectWithTag("Boss").transform;
                transform.localPosition = new Vector3(-1f, 1f, 0f);
                movingToStandardPosition = false;
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().bossDroneCR = false;
            }
        }
    }

    public void SpawnSpear()
    {
        anim.SetTrigger("Spear");
    }

    void SpawnSpearAnim()
    {
        BossAudioManager.BAM.playClip[2] = true;
        BossAudioManager.BAM.playClip[0] = true;
        GameObject instance = Instantiate(spear, transform.position, Quaternion.identity) as GameObject;
        instance.GetComponent<SpearController>().direction = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    public IEnumerator ChangingPositionOfDrone(bool dronInMove, Vector2 destination)
    {
        anim.enabled = false;
        Debug.Log(destination);
        if (dronInMove == false) //Going back to player
        {
            //transform.localPosition = destination;
            destinationPoint = destination;
            dronIsMoving = dronInMove;
        }
        else if (dronInMove == true) //Going to chosen point
        {
            //transform.position = destination;
            transform.parent = null;
            destinationPoint = destination;
            dronIsMoving = dronInMove;
        }
        dronOnTheWay = true;
        yield return null;
    }

    public void MovingBackToStandard()
    {
        anim.enabled = false;
        movingToStandardPosition = true;
    }
}