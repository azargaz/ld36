﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossController : MonoBehaviour
{
    enum bossState { idle, throwingSpear, walking, charge, meleeAttack, shield, teleport}
    [SerializeField]bossState curBossState = bossState.idle;

    Collider2DController colliderController;

    Bounds bounds;
    Vector2 leftTop;
    Vector2 rightTop;
    [SerializeField]
    LayerMask playerMask;

    [SerializeField]
    GameObject spearLeft;
    [SerializeField]
    GameObject spearRight;

    [SerializeField]
    GameObject drone;
    [SerializeField]
    GameObject darkShield;

    float gravity = -20f;
    private Vector3 playerPosition;
    public float timeToWalk;
    int direction = 1;
    public float chargeSpeed;
    public float walkingSpeed;
    public float distanceToCharge; //If distance to player is equal or smaller then enemy is charging
    public float switchingStateIdleTime; //Time between changing states
    public int numberOfSpawnedSpears;
    public float waitingTimeBetweenSpears;
    public float shieldDuration;

    private bool stopWalkCR = false;
    [HideInInspector] public bool stopChargeCR = false;
    private bool stopMovement = false;
    private bool invulnerable = false;

    public float maxHealth;
    [HideInInspector]public float curHealth;
    private float shieldActivationHPLimit;
    public GameObject[] teleportPoints;

    private List<GameObject> activeTeleports = new List<GameObject>();
    private List<GameObject> inactiveTeleports = new List<GameObject>();
    /***************/

    [SerializeField]
    GameObject deathParticles;
    Animator anim;

	void Start ()
    {
        GetComponent<Animator>();
        colliderController = gameObject.GetComponent<Collider2DController>();
        bounds = gameObject.GetComponent<BoxCollider2D>().bounds;
        curHealth = maxHealth;
        shieldActivationHPLimit = 0.75f * maxHealth;
        Debug.Log("ShieldActivation: " + shieldActivationHPLimit);
        for (int i = 0; i < teleportPoints.Length; i++)
        {
            inactiveTeleports.Add(teleportPoints[i]);
        }

        for (int i = 0; i < inactiveTeleports.Count / 2; i++)
        {
            int index = Random.Range(0, inactiveTeleports.Count - 1);
            activeTeleports.Add(inactiveTeleports[index]);
            inactiveTeleports.RemoveAt(index);
        }
        SwitchingTeleportPoints();
    }
	
	void FixedUpdate ()
    {
        if (curBossState == bossState.idle)
        {
            CalculateBounds();
            RaycastHit2D leftHit = Physics2D.Raycast(leftTop, Vector2.left, 30f, playerMask);
            RaycastHit2D rightHit = Physics2D.Raycast(rightTop, Vector2.right, 30f, playerMask);

            Debug.DrawRay(leftTop, Vector2.left, Color.red, 1f);
            Debug.DrawRay(rightTop, Vector2.right, Color.red, 1f);

            if (leftHit || rightHit)
            {
                playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position - new Vector3((bounds.max.x - bounds.min.x), 0f, 0f);
                if (rightHit)
                {
                   // if (rightHit.distance <= distanceToCharge)
                   // {
                        direction = 1;
                        curBossState = bossState.charge;
                        spearRight.SetActive(true);
                   // }
                   // else
                   // {
                    //    direction = 1;
                    //    curBossState = bossState.walking;
                    //}
                }
                else if (leftHit)
                {
                    //if (leftHit.distance <= distanceToCharge)
                    //{
                        direction = -1;
                        curBossState = bossState.charge;
                        spearLeft.SetActive(true);
                    //}
                    /*else
                    {
                        direction = -1;
                        curBossState = bossState.walking;
                    }*/
                }
            }
            else
            {
                curBossState = bossState.throwingSpear;
                StartCoroutine(ThrowingSpear());
            }
        }
        else if (curBossState == bossState.charge)
        {
            //Debug.Log(playerPosition);
            if (stopMovement == false)
                transform.Translate(new Vector2(direction * chargeSpeed * Time.deltaTime, 0f));
            if (direction == 1 && transform.position.x >= playerPosition.x)
            {
                if (stopChargeCR == false)
                    StartCoroutine(StoppingCharging());
            }
            else if (direction == -1 && transform.position.x <= playerPosition.x)
            {
                if (stopChargeCR == false)
                    StartCoroutine(StoppingCharging());
            }
        }
        else if (curBossState == bossState.walking)
        {
            if (stopMovement == false)
                transform.Translate(new Vector2(direction * walkingSpeed * Time.deltaTime, 0f));
            if (stopWalkCR == false)
                StartCoroutine(StoppingWalking());

            if (direction == 1 && transform.position.x >= playerPosition.x && walkingSpeed != 0)
            {
                StopCoroutine(StoppingWalking());
                stopWalkCR = false;
                stopMovement = false;
                curBossState = bossState.teleport;
                StartCoroutine(Teleporting());
            }
            else if (direction == -1 && transform.position.x <= playerPosition.x && walkingSpeed != 0)
            {
                StopCoroutine(StoppingWalking());
                stopWalkCR = false;
                stopMovement = false;
                curBossState = bossState.teleport;
                StartCoroutine(Teleporting());
            }
        }

        if (curHealth <= shieldActivationHPLimit)
        {
            Debug.Log("ShieldActivation: " + shieldActivationHPLimit);
            if (shieldActivationHPLimit == 0.75f * maxHealth)
                shieldActivationHPLimit = 0.5f * maxHealth;
            else if (shieldActivationHPLimit == 0.5f * maxHealth)
                shieldActivationHPLimit = 0.25f * maxHealth;
            else if (shieldActivationHPLimit == 0.25f * maxHealth)
                shieldActivationHPLimit = -100;

            StartCoroutine(DarkShield());
        }

        colliderController.Move(new Vector3(0f, gravity, 0f));
	}

    void CalculateBounds()
    {
        bounds = gameObject.GetComponent<BoxCollider2D>().bounds;
        leftTop = new Vector2(bounds.max.x, bounds.max.y);
        rightTop = bounds.max;
    }

    public void TakingDamage(float damage)
    {
        Debug.Log("takingDamageBoss " + curHealth);
        if (invulnerable == false)
        {
            curHealth -= damage;
            ClearingVariablesForTeleport();
            StartCoroutine(Teleporting());
            if (curHealth <= 0)
            {
                MenuManager.mm.bossDead = true;
                Instantiate(deathParticles, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }

    public IEnumerator StoppingWalking()
    {
        stopWalkCR = true;
        yield return new WaitForSeconds(timeToWalk);

        stopMovement = true;

        yield return new WaitForSeconds(switchingStateIdleTime);
        stopWalkCR = false;
        stopMovement = false;

        curBossState = bossState.teleport;
        StartCoroutine(Teleporting());

        yield return null;
    }

    public IEnumerator StoppingCharging()
    {
        stopChargeCR = true;
        spearLeft.SetActive(false);
        spearRight.SetActive(false);

        stopMovement = true;
        Debug.Log("STOP CHARGING");
        yield return new WaitForSeconds(switchingStateIdleTime);
        stopChargeCR = false;
        stopMovement = false;
        Debug.Log("STOP CHARGING");
        StartCoroutine(MeleeWithIdle());

        yield return null;
    }

    public IEnumerator Teleporting()
    {
        if (invulnerable == false)
        {
            Debug.Log("TELEPORT");
            SwitchingTeleportPoints();
            int index = Random.Range(0, activeTeleports.Count - 1);
            Vector3 teleportingPosition = activeTeleports[index].transform.position;
            float distanceBetween = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, teleportingPosition);

            Debug.Log(distanceBetween);
            if (distanceBetween <= 1f)
            {
                teleportingPosition = activeTeleports[(index + 1) % activeTeleports.Count - 1].transform.position;
                transform.position = teleportingPosition;
            }
            else
                transform.position = teleportingPosition;

            yield return new WaitForSeconds(switchingStateIdleTime);

            curBossState = bossState.idle;

            yield return null;
        }
        else
            curBossState = bossState.idle;
            yield return null;
    }

    public IEnumerator ThrowingSpear()
    {
        for (int i = 0; i < numberOfSpawnedSpears; i++)
        {
            drone.GetComponent<BossCombatDrone>().SpawnSpear();            
            yield return new WaitForSeconds(waitingTimeBetweenSpears);
        }

        drone.GetComponent<BossCombatDrone>().anim.enabled = false;
        drone.GetComponent<BossCombatDrone>().MovingBackToStandard();
        yield return new WaitUntil(() => drone.GetComponent<BossCombatDrone>().anim.enabled == true);

        curBossState = bossState.teleport;
        StartCoroutine(Teleporting());

        yield return null;
    }

    public IEnumerator DarkShield()
    {
        invulnerable = true;
        darkShield.SetActive(true);
        yield return new WaitForSeconds(shieldDuration);

        invulnerable = false;
        darkShield.GetComponent<Animator>().SetTrigger("Destroy");
        yield return new WaitForSeconds(darkShield.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);

        darkShield.SetActive(false);

        yield return null;
    }

    public IEnumerator MeleeWithTeleport()
    {
        ClearingVariablesForTeleport();
        curBossState = bossState.meleeAttack;

        drone.GetComponent<BossCombatDrone>().anim.enabled = false;
        StartCoroutine(drone.GetComponent<BossCombatDrone>().ChangingPositionOfDrone(false, transform.position));
        yield return new WaitUntil(() => drone.GetComponent<BossCombatDrone>().anim.enabled == true);

        //TODO -- Melee EXPLOSION!!!!!
        yield return new WaitForSeconds(1.5f); //Tymczasowo żeby był efekt, że niby coś się tutaj dzieje, nie mam już siły...

        drone.GetComponent<BossCombatDrone>().anim.enabled = false;
        drone.GetComponent<BossCombatDrone>().MovingBackToStandard();
        yield return new WaitUntil(() => drone.GetComponent<BossCombatDrone>().anim.enabled == true);

        curBossState = bossState.teleport;
        StartCoroutine(Teleporting());

        yield return null;
    }

    public IEnumerator MeleeWithIdle()
    {
        ClearingVariablesForTeleport();
        curBossState = bossState.meleeAttack;

        drone.GetComponent<BossCombatDrone>().anim.enabled = false;
        StartCoroutine(drone.GetComponent<BossCombatDrone>().ChangingPositionOfDrone(false, transform.position));
        yield return new WaitUntil(() => drone.GetComponent<BossCombatDrone>().anim.enabled == true);

        //TODO -- Melee EXPLOSION!!!!!
        yield return new WaitForSeconds(1.5f); //Tymczasowo żeby był efekt, że niby coś się tutaj dzieje, nie mam już siły...

        drone.GetComponent<BossCombatDrone>().anim.enabled = false;
        drone.GetComponent<BossCombatDrone>().MovingBackToStandard();
        yield return new WaitUntil(() => drone.GetComponent<BossCombatDrone>().anim.enabled == true);

        curBossState = bossState.idle;

        yield return null;
    }

    void ClearingVariablesForTeleport()
    {
        StopCoroutine(ThrowingSpear());
        StopCoroutine(StoppingCharging());
        StopCoroutine(StoppingWalking());
        StopCoroutine(Teleporting());
        curBossState = bossState.teleport;
        invulnerable = false;
        stopChargeCR = false;
        stopWalkCR = false;
        stopMovement = false;
        spearLeft.SetActive(false);
        spearRight.SetActive(false);
    }

    void SwitchingTeleportPoints()
    {
        int index = Random.Range(0, inactiveTeleports.Count - 1);
        activeTeleports.Add(inactiveTeleports[index]);
        inactiveTeleports.RemoveAt(index);

        for (int i = 0; i < activeTeleports.Count; i++)
        {
            activeTeleports[i].SetActive(true);
        }
        for (int i = 0; i < inactiveTeleports.Count; i++)
        {
            inactiveTeleports[i].SetActive(false);
        }
    }
}