﻿using UnityEngine;
using System.Collections;

public class MeleeAttackZone : MonoBehaviour
{
    [HideInInspector]
    public bool CRisReady = true;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (CRisReady == true)
            {
                CRisReady = false;
                StartCoroutine(gameObject.transform.parent.root.GetComponent<BossController>().MeleeWithTeleport());
            }
        }
    }
}