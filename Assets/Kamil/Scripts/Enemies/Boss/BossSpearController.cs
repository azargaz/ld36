﻿using UnityEngine;
using System.Collections;

public class BossSpearController : MonoBehaviour
{
    public float damage;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            collider.gameObject.GetComponent<PlayerStats>().curHealth -= damage;
            if (collider.gameObject.GetComponent<PlayerStats>().curHealth <= 0)
            {
                Debug.Log("DEATH");
            }
            Debug.Log("DAMAGE");
            gameObject.transform.parent.GetComponent<BossSpearController>().EnteredOnPlayerWithCharge(collider);
        }
    }

    public void EnteredOnPlayerWithCharge(Collider2D collider)
    {
        if (gameObject.transform.parent.root.GetComponent<BossController>().stopChargeCR == false)
            StartCoroutine(gameObject.transform.parent.root.GetComponent<BossController>().StoppingCharging());
    }
}