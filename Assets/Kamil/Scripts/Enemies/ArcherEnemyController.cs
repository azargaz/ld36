﻿using UnityEngine;
using System.Collections;

public class ArcherEnemyController : ArrowController
{
    [SerializeField] GameObject arrow;
    public float spawningArrowTime;

	void Start ()
    {
        SpawningArrow();
	}
	
	void FixedUpdate ()
    {
        
	}

    void SpawningArrow()
    {
        if (GameObject.FindGameObjectWithTag("Player") == null)
            return;

        direction = (GameObject.FindGameObjectWithTag("Player").transform.position.x > transform.position.x ? 1 : -1);

        GetComponent<SpriteRenderer>().flipX = (direction == 1) ? false : true;

        GameObject instance = Instantiate(arrow, transform.position, Quaternion.identity) as GameObject;
        instance.GetComponent<ArrowController>().speed = speed;
        instance.GetComponent<ArrowController>().timeToLive = timeToLive;
        instance.GetComponent<ArrowController>().direction = direction;
        instance.GetComponent<ArrowController>().damage = damage;
        //instance.GetComponent<ArrowController>().destructionMask = destructionMask;
        Invoke("SpawningArrow", spawningArrowTime);
    }
}