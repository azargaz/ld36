﻿using UnityEngine;
using System.Collections;

public class BossAudioManager : MonoBehaviour
{
    [SerializeField]
    AudioClip[] clip;
    [SerializeField]
    AudioSource[] audioPlayers;
    public bool[] playClip;
    public static BossAudioManager BAM;

    void Start()
    {
        BAM = this;

        for (int i = 0; i < clip.Length; i++)
        {
            audioPlayers[i].clip = clip[i];
        }
    }

    void Update()
    {
        for (int i = 0; i < playClip.Length; i++)
        {
            if (playClip[i])
            {
                playClip[i] = false;
                audioPlayers[i].Play();
            }
        }
    }
}
