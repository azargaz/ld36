﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour
{
    public float maxHealth;
    [HideInInspector] public float curHealth;

    public void Start()
    {
        curHealth = maxHealth;
    }

    public void TakingDamage(float damage)
    {
        curHealth -= damage;

        StartCoroutine(DamageFlashing());       

        if (curHealth <= 0)
        {
            //TODO -- niszczenie przeciwnika, gdy jego poziom zdrowia wynosi 0
            Destroy(gameObject);
        }
    }

    IEnumerator DamageFlashing()
    {
        Color mobColor = GetComponent<SpriteRenderer>().color;

        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);

        yield return new WaitForSeconds(0.1f);

        GetComponent<SpriteRenderer>().color = mobColor;
    }
}