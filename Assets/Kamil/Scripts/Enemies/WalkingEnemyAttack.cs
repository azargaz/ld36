﻿using UnityEngine;
using System.Collections;

public class WalkingEnemyAttack : MonoBehaviour {

	void Start ()
    {

    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            {
                float damage = gameObject.transform.parent.GetComponent<WalkingEnemyController>().damage;
                int movementDirection = gameObject.transform.parent.GetComponent<WalkingEnemyController>().movementDirection;
                collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
                //collider.transform.position = Vector3.Lerp(collider.transform.position, new Vector3(2f * movementDirection, 0f, 0f), 1f);
            }
        }
    }
}
