﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{   
    public static MenuManager mm;
    public bool bossDead;
    bool GameBeaten;
    [SerializeField]
    GameObject congrats;
    GameObject clone;

    void Start()
    {              
        mm = this;
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if(bossDead)
        {
            Invoke("End", 2f);
            bossDead = false;
        }

        if(GameBeaten && SceneManager.GetActiveScene().buildIndex == 0)
        {
            clone = Instantiate(congrats);
            GameBeaten = false;            
        }    
        
        if(clone != null)
        {
            if (Input.anyKeyDown)
            {
                Destroy(clone);
            }
        }    
    }    

    void End()
    {
        SceneManager.LoadScene(0);
        GameBeaten = true;
    }

    public void Death()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }  
}
