﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public float maxHealth;
    public float curHealth;

    [SerializeField]
    GameObject deathParticles;

    [SerializeField]
    Image HP_bar;

	void Start ()
    {
        curHealth = maxHealth;
	}

    void Update()
    {
        if(curHealth < 0)
        {
            curHealth = 0;
        }
        else if(curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

        if(HP_bar != null)
            HP_bar.fillAmount = Mathf.Lerp(HP_bar.fillAmount, curHealth / maxHealth, Time.deltaTime * 10);
    }
	
	public void TakingDamage(float damage)
    {
        curHealth -= damage;

        StartCoroutine(DamageFlashing());

        if (curHealth <= 0)
        {
            if(MenuManager.mm != null)
            {
                MenuManager.mm.Invoke("Death", 1f);
                gameObject.SetActive(false);

                if (deathParticles != null)
                    Instantiate(deathParticles, transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));
                else
                    Debug.LogError("Nie ma deathParticles gracza");
            }
        }
    }

    IEnumerator DamageFlashing()
    {
        Color playerColor = GetComponent<SpriteRenderer>().color;

        GetComponent<SpriteRenderer>().color = new Color(1, 0, 0);

        yield return new WaitForSeconds(0.1f);

        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }
}