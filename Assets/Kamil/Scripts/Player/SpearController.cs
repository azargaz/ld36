﻿using UnityEngine;
using System.Collections;

public class SpearController : MonoBehaviour
{
    public float speed;
    public Vector3 direction;
    public float gravity;
    public float timeToLive;
    public float damage;

    BoxCollider2D col;
    Animator anim;

	void Start ()
    {
        col = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        Vector3 vectorToTarget = direction - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = q;

        StartCoroutine(DestroyingSpear());
    }
	
	void FixedUpdate ()
    {
        if (direction != Vector3.zero)
            transform.Translate(new Vector3(speed, gravity, 0f) * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ground")
        {
            StopAndDestroySpear();
        }
        else if (gameObject.name.Contains("LightSpear") && collider.tag == "Boss")
        {
            collider.gameObject.GetComponent<BossController>().TakingDamage(damage);
            StopAndDestroySpear();
        }
        else if (gameObject.name.Contains("LightSpear") && collider.tag == "Enemy")
        {
            //TODO -- system obrażeń przeciwników
            if (collider.gameObject.name.Contains("Vulnerable") && collider.gameObject.transform.parent.name.Contains("WalkingEnemy"))
            {
                collider.gameObject.transform.parent.GetComponent<EnemyStats>().TakingDamage(damage);
            }
            else
                collider.gameObject.GetComponent<EnemyStats>().TakingDamage(damage);

            StopAndDestroySpear();
        }
        else if (gameObject.name.Contains("LightSpear") && collider.tag == "Undestructable")
        {
            StopAndDestroySpear();
        }

        else if (gameObject.name.Contains("DarkSpear") && collider.tag == "Player")
        {
            collider.gameObject.GetComponent<PlayerStats>().TakingDamage(damage);
            StopAndDestroySpear();
        }
        else if (collider.tag == "Shield" && !gameObject.name.Contains("DarkSpear"))
        {
            StopAndDestroySpear();
        }
    }

    IEnumerator DestroyingSpear()
    {
        yield return new WaitForSeconds(timeToLive);
        col.enabled = false;
        anim.SetTrigger("Destroy");
        yield return null;
    }

    #region Dont touch

    void StopAndDestroySpear()
    {
        direction = Vector3.zero;
        col.enabled = false;
        AudioManager.AM.playClip[1] = true;
        anim.SetTrigger("Destroy");
    }    

    void DestroySpear()
    {        
        Destroy(gameObject);
    }

    #endregion
}