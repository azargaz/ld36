﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AoeAttackDrone : MonoBehaviour
{

    List<GameObject> damagedObjects = new List<GameObject>();
    public float damage;
    public float timeToLive;

	void Start ()
    {
	    
	}
	
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Boss" && !damagedObjects.Contains(collider.gameObject))
        {
            collider.gameObject.GetComponent<BossController>().TakingDamage(damage);
            damagedObjects.Add(collider.gameObject);
        }
        else if (collider.tag == "Enemy" && !damagedObjects.Contains(collider.gameObject))
        {
            if (collider.name.Contains("Vulnerable"))
                collider.gameObject.transform.parent.GetComponent<EnemyStats>().TakingDamage(damage);
            else
                collider.gameObject.GetComponent<EnemyStats>().TakingDamage(damage);
            damagedObjects.Add(collider.gameObject);
        }
    }

    public IEnumerator Destroying()
    {
        yield return new WaitForSeconds(timeToLive);
        //Destroy(gameObject);
        damagedObjects = new List<GameObject>();
        gameObject.SetActive(false);

        yield return null;
    }
}