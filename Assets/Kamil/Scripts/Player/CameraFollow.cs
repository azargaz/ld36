﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    Transform target;

	void FixedUpdate ()
    {
        transform.position = Vector3.Slerp(transform.position, new Vector3(target.position.x, target.position.y - 1, -10), Time.fixedDeltaTime * 10);
	}
}
