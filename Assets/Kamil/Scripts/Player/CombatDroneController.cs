﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CombatDroneController : MonoBehaviour
{
    [SerializeField]
    GameObject spear;
    [SerializeField]
    Transform firePoint;
    [SerializeField]
    GameObject aoeArea;

    [Header("Attack CDs/Costs")]

    [SerializeField]
    float spearBatteryCost;
    [SerializeField]
    float spearCooldown;
    float timeToNextSpear;

    [SerializeField]
    float aoeBatteryCost;
    [SerializeField]
    float aoeAttackCooldown;
    float timeToNextAoE;

    [Header("Drone Battery")]

    [SerializeField]
    float droneMaxBattery;
    float droneCurBattery;
    [SerializeField]
    float batteryRegenAmount;
    [SerializeField]
    float batteryRegenDelay;
    float batteryRegenCd;

    [Header("Other")]

    [SerializeField]
    Image batteryBar;
    [SerializeField]
    Animator player;
    Vector3 direction;
    Camera mainCamera;
    Animator anim;

    private bool dronIsMoving = false; //If false, drone is going back to player;; If true, drone is going to chosen point
    private bool dronOnTheWay = false; //Checking if drone is currently moving to player or to chosen point
    private Vector3 localPosition;
    private Vector3 destinationPoint;
    private Transform playerPosition;
    [SerializeField]
    float droneSpeed;
    /*************/
	void Start ()
    {
        droneCurBattery = droneMaxBattery;
	    mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
        anim = GetComponent<Animator>();
        localPosition = transform.localPosition;
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update ()
    {
        #region Drone battery

        if (droneCurBattery < 0)
        {
            droneCurBattery = 0;
        }
        else if(droneCurBattery > droneMaxBattery)
        {
            droneCurBattery = droneMaxBattery;
        }

        if(batteryRegenCd <= Time.time && droneCurBattery < droneMaxBattery)
        {
            droneCurBattery += Time.deltaTime * batteryRegenAmount;
        }

        if(batteryBar != null)
            batteryBar.fillAmount = Mathf.Lerp(batteryBar.fillAmount, droneCurBattery / droneMaxBattery, Time.deltaTime * 8);

        #endregion

        Vector3 mousePos = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, -(mousePos - transform.position));

        if (Input.GetButton("Fire1") && timeToNextSpear < Time.time && droneCurBattery >= spearBatteryCost)
        {
            timeToNextSpear = Time.time + spearCooldown;
            droneCurBattery -= spearBatteryCost;
            batteryRegenCd = Time.time + batteryRegenDelay;            

            anim.SetTrigger("Spear");
            player.SetTrigger("Attack");
        }
        else if (Input.GetButton("Fire2") && timeToNextAoE < Time.time && droneCurBattery >= aoeBatteryCost)
        {
            AudioManager.AM.playClip[3] = true;
            timeToNextAoE = Time.time + aoeAttackCooldown;
            droneCurBattery -= aoeBatteryCost;
            batteryRegenCd = Time.time + batteryRegenDelay;

            aoeArea.SetActive(true);
            StartCoroutine(aoeArea.GetComponent<AoeAttackDrone>().Destroying());
            //TODO -- AoE attack
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            anim.enabled = false;
            //if (dronIsMoving == false)
            //{
            dronIsMoving = true;
            Vector3 mousePosition = Input.mousePosition;
            mousePosition = mainCamera.ScreenToWorldPoint(mousePosition);
            StartCoroutine(ChangingPositionOfDrone(dronIsMoving, mousePosition));
            //}
            /*
            else if (dronIsMoving == true)
            {
                dronIsMoving = false;
                StartCoroutine(ChangingPositionOfDrone(dronIsMoving, localPosition));
            }*/
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            anim.enabled = false;
            dronIsMoving = false;
            StartCoroutine(ChangingPositionOfDrone(dronIsMoving, localPosition));
        }
	}

    void FixedUpdate()
    {
        if (dronOnTheWay == true)
        {
            if (dronIsMoving == false)
                destinationPoint = playerPosition.position + new Vector3(-1f, 1f, 0f);
            transform.position = Vector3.MoveTowards(transform.position, destinationPoint, droneSpeed * Time.deltaTime);

            //If moving is done
            if (transform.position == destinationPoint)
            {
                dronOnTheWay = false;
                //Debug.Log("CCCP");
                anim.enabled = true;
                if (dronIsMoving == false)
                {
                    transform.parent = GameObject.FindGameObjectWithTag("Player").transform;
                    transform.localPosition = new Vector3(-1f, 1f, 0f);
                }
                else if (dronIsMoving == true)
                {
                    transform.parent = null;
                }
            }
        }

        if (!gameObject.GetComponentInChildren<SpriteRenderer>().isVisible)
        {
            dronIsMoving = false;
            dronOnTheWay = true;
        }
    }

    void SpawnSpear()
    {
        AudioManager.AM.playClip[2] = true;
        AudioManager.AM.playClip[0] = true;
        GameObject instance = Instantiate(spear, firePoint.position, Quaternion.identity) as GameObject;
        direction = Input.mousePosition;
        direction = mainCamera.ScreenToWorldPoint(direction);
        instance.GetComponent<SpearController>().direction = direction;
    }

    IEnumerator ChangingPositionOfDrone(bool dronInMove, Vector2 destination)
    {
        Debug.Log(destination);
        if (dronInMove == false) //Going back to player
        {
            //transform.localPosition = destination;
            destinationPoint = destination;
        }
        else if (dronInMove == true) //Going to chosen point
        {
            //transform.position = destination;
            transform.parent = null;
            destinationPoint = destination;
        }
        dronOnTheWay = true;
        yield return null;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ground" && dronIsMoving == true)
        {
            dronOnTheWay = false;
            anim.enabled = true;
        }
    }
}