﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2DController))]
public class PlayerController : MonoBehaviour
{

    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    public float moveSpeed = 6;

    float gravity;
    float loweredGravity;
    public float loweredGravityMultiplier;
    float jumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    Collider2DController controller;

    private bool secondJumpAvailable = true;

    private Vector2 middleOfPlayer;
    public LayerMask groundMask;
    Animator anim;

    [SerializeField]
    GameObject drone;

    [HideInInspector] public bool bossDroneCR = false;
    [HideInInspector] public bool invulnerableToDarkness = false;
    /***************/

    void Start()
    {
        controller = GetComponent<Collider2DController>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        loweredGravity = gravity / loweredGravityMultiplier;
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetButton("Jump") && !controller.collisions.below)
        {
            if (velocity.y < 0)
                gravity = loweredGravity;
        }
        else
            gravity = loweredGravity * loweredGravityMultiplier;
    }

    void FixedUpdate()
    {
        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
            secondJumpAvailable = true;
        }

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        #region Animations

        // Walk & jump
        anim.SetBool("Grounded", controller.collisions.below);
        anim.SetFloat("InputX", Mathf.Abs(input.x));      

        // Flip sprite
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        sprite.flipX = (input.x == 0) ? sprite.flipX : (input.x > 0) ? false : true;        

        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Player_attack") || anim.GetCurrentAnimatorStateInfo(0).IsName("Player_attack_airborne"))
        {
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Player_attack"))
                input.x = 0;

            float mousePosX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;

            if(mousePosX > drone.transform.position.x)
            {
                sprite.flipX = false;
            }
            else
            {
                sprite.flipX = true;
            }
        }

        //TODO - tymczasowo wyłączyłem dust, bo były bugi //Kamil
        //sam żeś jest bug //Hubert
        if(transform.FindChild("Dust").gameObject != null)
        {
            GameObject dust = transform.FindChild("Dust").gameObject;
            dust.transform.eulerAngles = new Vector3(0, (sprite.flipX) ? 90 : -90, 0);

            if (input.x != 0 && controller.collisions.below)
            {
                dust.GetComponent<ParticleSystem>().startLifetime = 0.3f;
            }
            else
            {
                dust.GetComponent<ParticleSystem>().startLifetime = 0f;
            }
        }    
        else
        {
            Debug.LogError("Nie ma dziecka 'Dust'.");
        }    

        #endregion

        if (Input.GetButtonDown("Jump") && controller.collisions.below)
        {
            velocity.y = jumpVelocity;
        }
        else if (Input.GetButtonDown("Jump") && !controller.collisions.below && secondJumpAvailable)
        {
            velocity.y = jumpVelocity;
            secondJumpAvailable = false;
        }

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (GameObject.FindGameObjectWithTag("Boss"))
        {
            Bounds bounds = gameObject.GetComponent<BoxCollider2D>().bounds;
            middleOfPlayer = new Vector2(bounds.min.x + (bounds.max.x - bounds.min.x) / 2, bounds.min.y + (bounds.max.y - bounds.min.y) / 2);

            Vector2 difference = new Vector2(GameObject.FindGameObjectWithTag("Boss").transform.position.x - transform.position.x,
                 GameObject.FindGameObjectWithTag("Boss").transform.position.y - transform.position.y);

            float distance = Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("Boss").transform.position);

            RaycastHit2D hit = Physics2D.Raycast(middleOfPlayer, difference, distance, groundMask);

            Debug.DrawRay(middleOfPlayer, difference, Color.red, 0.1f);
            if (hit && bossDroneCR == false)
            {
                bossDroneCR = true;
                StartCoroutine(GameObject.Find("CombatDroneBoss").GetComponent<BossCombatDrone>().ChangingPositionOfDrone(true, transform.position));
            }
        }
    }
}