﻿using UnityEngine;
using System.Collections;

public class PulseLightScript : MonoBehaviour
{
    public float increasingRadiusSize;
    public float maximumRadiusSize;
    public float minimumRadiusSize;

    CircleCollider2D circle = new CircleCollider2D();
    new Light light = new Light();

	void Start ()
    {
        light = gameObject.GetComponent<Light>();
        circle = gameObject.GetComponent<CircleCollider2D>();
        circle.radius = minimumRadiusSize;
        light.range = minimumRadiusSize + 0.8f;
	}
	
	void FixedUpdate ()
    {
        circle.radius = circle.radius * (1 + increasingRadiusSize);
        light.range = light.range * (1 + increasingRadiusSize);

        if (circle.radius > maximumRadiusSize || circle.radius < minimumRadiusSize)
        {
            increasingRadiusSize *= -1;
        }
	}

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.GetComponent<PlayerController>().invulnerableToDarkness = true;
            Debug.Log("Stay");
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.GetComponent<PlayerController>().invulnerableToDarkness = false;
            Debug.Log("Exit");
        }
    }
}